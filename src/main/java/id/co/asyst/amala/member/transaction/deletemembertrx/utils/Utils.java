package id.co.asyst.amala.member.transaction.deletemembertrx.utils;

import com.google.gson.Gson;
import id.co.asyst.commons.utils.constant.CommonsConstants;
import id.co.asyst.commons.utils.formatter.Responses;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import java.lang.reflect.Array;
import java.rmi.MarshalledObject;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
	private static final Logger log = LogManager.getLogger(Responses.class);

	private static final Gson gson = new Gson();

	public void validateRequest(Exchange exchange){
		String trxidStatus = exchange.getProperty("trxidStatus").toString();
		String memberid = exchange.getProperty("memberid").toString();
		ArrayList<String> trxid = new ArrayList<>();
		String resMsg = "";

		if (memberid.equals("null") || StringUtils.isEmpty(memberid))
			resMsg = "memberid is required";
		else {
			if (!trxidStatus.equals("null")) {
				trxid = exchange.getProperty("trxid", ArrayList.class);
				if (trxid.size() > 0) {
					for (int i = 0; i < trxid.size(); i++) {
						for (int j = i + 1 ; j < trxid.size(); j++) {
							if (trxid.get(i).equals(trxid.get(j)))
								resMsg = "duplicate trxid "+trxid.get(i);
						}
					}
					if (trxid.size() == 1) {
						if (StringUtils.isEmpty(trxid.get(0)))
							trxid = new ArrayList<>();
					}
				}
			}
		}
		exchange.setProperty("resMsg", resMsg);
		exchange.setProperty("trxid", trxid);
		exchange.setProperty("trxidReq", trxid);
	}

	public void generateDateTimeNow(Exchange exchange){
		Date date = new Date();
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd");
		String srtDate = sm.format(date);
		exchange.setProperty("dateTimeNow",srtDate);
		String srtDateFormat = smf.format(date);
		exchange.setProperty("dateNow",srtDateFormat);
	}

	public void saveMemAccID(Exchange exchange){
		List<Map<String, Object>> memberAccountRetrieve = exchange.getProperty("memberaccountRetrieve", List.class);
		exchange.setProperty("memberaccountid", memberAccountRetrieve.get(0).get("memberaccountid"));
	}

	public void saveTrxidAndTrxType(Exchange exchange){
		List<Map<String, Object>> membertrxRetrieve = exchange.getProperty("membertrxretrieve", List.class);
		exchange.setProperty("trxid", membertrxRetrieve.get(0).get("trxid"));
		exchange.setProperty("trxtype", membertrxRetrieve.get(0).get("trxtype"));
	}

	public void saveTrxDetail(Exchange exchange){
		List<Map<String, Object>> membertrxdetailretrieve = exchange.getProperty("membertrxdetailretrieve", List.class);
	}

	public void savemembertrxdetailretrieve(Exchange exchange){
		List<Map<String, Object>> membertrxdetailretrieve = exchange.getProperty("membertrxdetailretrieve", List.class);
		for (Map<String, Object> memtrxdetail : membertrxdetailretrieve) {
			memtrxdetail.put("awardmiles", Integer.parseInt(memtrxdetail.get("awardmiles").toString()) * -1);
			memtrxdetail.put("tiermiles", Integer.parseInt(memtrxdetail.get("tiermiles").toString()) * -1);
			memtrxdetail.put("frequency", Integer.parseInt(memtrxdetail.get("frequency").toString()) * -1);
		}
		exchange.setProperty("membertrxdetailretrieve", membertrxdetailretrieve);
	}

	public void savemembertrxdetailadd(Exchange exchange){
		List<Map<String, Object>> membertrxdetailall = exchange.getProperty("membertrxdetailall", List.class);
		List<Map<String, Object>> membertrxdetaildata = exchange.getProperty("membertrxcompare", List.class);
		for (Map<String, Object> membertrxdet : membertrxdetaildata) {
			membertrxdet.put("awardmiles", (Integer.parseInt(membertrxdet.get("awardmiles").toString())*-1));
			membertrxdet.put("tiermiles", (Integer.parseInt(membertrxdet.get("tiermiles").toString())*-1));
			membertrxdet.put("frequency", (Integer.parseInt(membertrxdet.get("frequency").toString())*-1));
			membertrxdetailall.add(membertrxdet);
		}
		exchange.setProperty("membertrxdetailall", membertrxdetailall);
	}

	public void trxNotFound(Exchange exchange) {
		ArrayList<String> trxid = exchange.getProperty("errorTrxid", ArrayList.class);
		trxid.add(exchange.getProperty("trxid").toString());
		exchange.setProperty("errorTrxid", trxid);
	}

	public void savecomponenttrxidfailed(Exchange exchange){
		ArrayList<String> trxid = new ArrayList<>();
		exchange.setProperty("errorTrxid", trxid);
	}

	public void savecomponentreqbytrx(Exchange exchange){
		List<Map<String, Object>> memberaccdetail = new ArrayList<>();
		List<Map<String, Object>> membertrxdetail = new ArrayList<>();
		List<Map<String, Object>> memberaccountdetailupdate = new ArrayList<>();
		List<Map<String, Object>> memberaccountdetailnotupdate = new ArrayList<>();
		List<Map<String, Object>> membertrxdetailall = new ArrayList<>();
		List<String> membertrxusedbysourcetrxid = new ArrayList<>();
		exchange.setProperty("membertrxdetail", membertrxdetail);
		exchange.setProperty("memberaccdetail", memberaccdetail);
		exchange.setProperty("memberaccountdetailupdate", memberaccountdetailupdate);
		exchange.setProperty("memberaccountdetailnotupdate", memberaccountdetailnotupdate);
		exchange.setProperty("membertrxusedbysourcetrxid", membertrxusedbysourcetrxid);
		exchange.setProperty("membertrxdetailall", membertrxdetailall);
	}

	public void savememberaccdetailretrieve(Exchange exchange) {
		List<Map<String, Object>> memberaccdetailretrieve = exchange.getProperty("memberaccdetailretrieve", List.class);
		List<Map<String, Object>> memberaccdetail = exchange.getProperty("memberaccdetail", List.class);
		Map<String, Object> memaccdet = new HashMap<>();
		Map<String, Object> membertrxdet = exchange.getProperty("memtrxdet", Map.class);
		memaccdet.put("accdetailid", memberaccdetailretrieve.get(0).get("accdetailid"));
		memaccdet.put("trxid", memberaccdetailretrieve.get(0).get("trxid"));
		memaccdet.put("awardmiles", memberaccdetailretrieve.get(0).get("awardmiles"));
		memaccdet.put("tiermiles", memberaccdetailretrieve.get(0).get("tiermiles"));
		memaccdet.put("frequency", memberaccdetailretrieve.get(0).get("frequency"));
//		memaccdet.put("awardmilesMemTrxdet", membertrxdet.get("awardmiles"));
//		memaccdet.put("tiermilesMemTrxdet", membertrxdet.get("tiermiles"));
//		memaccdet.put("frequencyMemTrxdet", membertrxdet.get("frequency"));

		memberaccdetail.add(memaccdet);
		exchange.setProperty("memberaccdetailretrieve", memberaccdetail);
	}

	public void savemembertrxretrieve(Exchange exchange){
		List<Map<String, Object>> membertransactionretrieve = exchange.getProperty("membertrxdetail", List.class);
		List<Map<String, Object>> membertransaction = exchange.getProperty("membertransactionretrieve", List.class);
		membertransactionretrieve.add(membertransaction.get(0));
		exchange.setProperty("membertransactionretrieve", membertransactionretrieve);
	}

	public void validatebalancemiles(Exchange exchange){
		/*memtrxdet dari process 1*/
		List<Map<String, Object>> membertrxdetailretrieve = exchange.getProperty("membertrxdetailretrieve", List.class);
		/*memtrxdet dari process 2&3*/
		List<Map<String, Object>> membertrxdetailall = exchange.getProperty("membertrxdetailall", List.class);
		/*memberaccdet dari process 2*/
		List<Map<String, Object>> memberaccdetailretrieve = exchange.getProperty("memberaccdetailretrieve", List.class);
		/*membertrx dari process 3*/
		List<Map<String, Object>> membertransactionretrieve = exchange.getProperty("membertransactionretrieve", List.class);

		List<Map<String, Object>> memberaccountdetailupdate = exchange.getProperty("memberaccountdetailupdate", List.class);
		List<Map<String, Object>> memberaccountdetailnotupdate = exchange.getProperty("memberaccountdetailnotupdate", List.class);

		String log = "data compare\n";
		for (Map<String, Object> memaccdet : memberaccdetailretrieve){
			int awardmilestrxdetall = 0;
			int tiermilestrxdetall = 0;
			int frequencytrxdetall = 0;
			Map<String, Object> memberupdate = new HashMap<>();
			for (Map<String, Object> memtrxdetall : membertrxdetailall) {
				if (memtrxdetall.get("accountdetailid").equals(memaccdet.get("accdetailid"))) {
					awardmilestrxdetall += Integer.parseInt(memtrxdetall.get("awardmiles").toString());
					tiermilestrxdetall += Integer.parseInt(memtrxdetall.get("tiermiles").toString());
					frequencytrxdetall += Integer.parseInt(memtrxdetall.get("frequency").toString());
				}
			}
			String status = "";
			for (Map<String, Object> membertrx : membertransactionretrieve) {
				if (membertrx.get("trxid").equals(memaccdet.get("trxid"))) {
					if (Integer.parseInt(membertrx.get("awardmiles").toString()) == (awardmilestrxdetall+Integer.parseInt(memaccdet.get("awardmiles").toString()))) {
						if (Integer.parseInt(membertrx.get("tiermiles").toString()) == (tiermilestrxdetall+Integer.parseInt(memaccdet.get("tiermiles").toString()))) {
							if (Integer.parseInt(membertrx.get("frequency").toString()) == (frequencytrxdetall+Integer.parseInt(memaccdet.get("frequency").toString()))) {
								status = "balance";
								log += "BALANCE\n"+"awardmilestrxdetailcomp = "+awardmilestrxdetall+" tiermilestrxdetcom = "+tiermilestrxdetall+" freqtrxdetcomp = "+frequencytrxdetall+"\n"+
										"awardmilesaccdet = "+memaccdet.get("awardmiles") + " tiermilesaccdet = "+memaccdet.get("tiermiles")+" freqaccdet = "+frequencytrxdetall+"\n"+
										"awardmilestrx = "+membertrx.get("awardmiles") + " tiermilestrx = "+membertrx.get("tiermiles")+" freqtrx = "+membertrx.get("frequency")+"\n";
							} else {
								status = "notbalance";
								log += "NOT BALANCE\n"+"awardmilestrxdetailcomp = "+awardmilestrxdetall+" tiermilestrxdetcom = "+tiermilestrxdetall+" freqtrxdetcomp = "+frequencytrxdetall+"\n"+
										"awardmilesaccdet = "+memaccdet.get("awardmiles") + " tiermilesaccdet = "+memaccdet.get("tiermiles")+" freqaccdet = "+frequencytrxdetall+"\n"+
										"awardmilestrx = "+membertrx.get("awardmiles") + " tiermilestrx = "+membertrx.get("tiermiles")+" freqtrx = "+membertrx.get("frequency")+"\n";
							}
						} else {
							status = "notbalance";
							log += "NOT BALANCE\n"+"awardmilestrxdetailcomp = "+awardmilestrxdetall+" tiermilestrxdetcom = "+tiermilestrxdetall+" freqtrxdetcomp = "+frequencytrxdetall+"\n"+
									"awardmilesaccdet = "+memaccdet.get("awardmiles") + " tiermilesaccdet = "+memaccdet.get("tiermiles")+" freqaccdet = "+frequencytrxdetall+"\n"+
									"awardmilestrx = "+membertrx.get("awardmiles") + " tiermilestrx = "+membertrx.get("tiermiles")+" freqtrx = "+membertrx.get("frequency")+"\n";
						}
					} else {
						status = "notbalance";
						log += "NOT BALANCE\n"+"awardmilestrxdetailcomp = "+awardmilestrxdetall+" tiermilestrxdetcom = "+tiermilestrxdetall+" freqtrxdetcomp = "+frequencytrxdetall+"\n"+
								"awardmilesaccdet = "+memaccdet.get("awardmiles") + " tiermilesaccdet = "+memaccdet.get("tiermiles")+" freqaccdet = "+frequencytrxdetall+"\n"+
								"awardmilestrx = "+membertrx.get("awardmiles") + " tiermilestrx = "+membertrx.get("tiermiles")+" freqtrx = "+membertrx.get("frequency")+"\n";
					}
				}
			}
			if (status == "balance") {
				for (Map<String, Object> membertrxdet : membertrxdetailretrieve) {
					if (membertrxdet.get("accountdetailid").equals(memaccdet.get("accdetailid"))) {
						memberupdate.put("accdetailid", memaccdet.get("accdetailid"));
						memberupdate.put("awardmiles", (Integer.parseInt(memaccdet.get("awardmiles").toString()) +Integer.parseInt(membertrxdet.get("awardmiles").toString())));
						memberupdate.put("tiermiles", (Integer.parseInt(memaccdet.get("tiermiles").toString()) +Integer.parseInt(membertrxdet.get("tiermiles").toString())));
						memberupdate.put("frequency", (Integer.parseInt(memaccdet.get("frequency").toString()) +Integer.parseInt(membertrxdet.get("frequency").toString())));
						memberaccountdetailupdate.add(memberupdate);
						log += "awardmilestrxdet = "+ membertrxdet.get("awardmiles") + " tiermilestrxdet = "+membertrxdet.get("tiermiles") + " freqtrxdet = "+membertrxdet.get("frequency")+"\n";
					}
				}
			} else {
				memberaccountdetailnotupdate.add(memaccdet);
			}
		}

		exchange.setProperty("statusBalance", log);
		exchange.setProperty("memberaccountdetailupdate", memberaccountdetailupdate);
		exchange.setProperty("memberaccountdetailnotupdate", memberaccountdetailnotupdate);

//		for (Map<String, Object> trx : membertransactionretrieve) {
//			int awardmilestrx = Integer.parseInt(trx.get("awardmiles").toString());
//			int tiermilestrx = Integer.parseInt(trx.get("tiermiles").toString());
//			int frequencytrx = Integer.parseInt(trx.get("frequency").toString());
//			for (Map<String, Object> accdet : memberaccdetailretrieve) {
//				if (accdet.get("trxid").equals(trx.get("trxid"))) {
//					int awardmilesaccdet = Integer.parseInt(accdet.get("awardmiles").toString());
//					int tiermilesaccdet = Integer.parseInt(accdet.get("tiermiles").toString());
//					int frequencyaccdet = Integer.parseInt(accdet.get("frequency").toString());
//					for (Map<String, Object> trxdet : membertrxdetailall) {
//						if (trxdet.get("accountdetailid").equals(accdet.get("accdetailid"))) {
//							int awardmilestrxdet = Integer.parseInt(trxdet.get("awardmiles").toString());
//							int tiermilestrxdet = Integer.parseInt(trxdet.get("tiermiles").toString());
//							int frequencytrxdet = Integer.parseInt(trxdet.get("frequency").toString());
//							if (((awardmilesaccdet+awardmilestrxdet) == awardmilestrx) && ((tiermilesaccdet+tiermilestrxdet) == tiermilestrx) && ((frequencyaccdet+frequencytrxdet) == frequencytrx)) {
//								for (Map<String, Object> trxdetailforupdate : membertrxdetailretrieve) {
//									Map<String, Object> memberupdate = new HashMap<>();
//									if (trxdetailforupdate.get("trxdetailid").equals(trxdet.get("trxdetailid"))) {
//										memberupdate.put("trxid", trx.get("trxid"));
//										memberupdate.put("accdetailid", accdet.get("accdetailid"));
//										memberupdate.put("awardmiles", (awardmilesaccdet+awardmilestrxdet));
//										memberupdate.put("tiermiles", (tiermilesaccdet+tiermilestrxdet));
//										memberupdate.put("frequency", (frequencyaccdet+frequencytrxdet));
//										memberaccountdetailupdate.add(memberupdate);
//										log += "\n   status Balance with \nProcess3 membertrx trxid {trxid} "+trx.get("trxid")+" awardmilestrx = "+awardmilestrx+" tiermilestrx = "+tiermilestrx+" frequencytrx = "+frequencytrx+
//												"\nProcess2 memberaccdet trxid {trxid} "+trx.get("trxid")+" and {accdetailid} "+accdet.get("accdetailid")+ " awardmilesaccdet = "+awardmilesaccdet+" tiermilesaccdet = "+tiermilesaccdet+" frequencyaccdet = "+frequencyaccdet+
//												"\nProcess1 membertrxdet accountdetailid {accountdetailid} "+accdet.get("accdetailid")+" and {trxid} "+trxdet.get("trxid")+ " awardmilestrxdet = "+awardmilestrxdet+" tiermilestrxdet = "+tiermilestrxdet+" frequencytrxdet = "+frequencytrxdet;
//									}
//								}
//							} else {
//								log += "\n   status not Balance with \nProcess3 membertrx trxid {trxid} "+trx.get("trxid")+" awardmilestrx = "+awardmilestrx+" tiermilestrx = "+tiermilestrx+" frequencytrx = "+frequencytrx+
//										"\nProcess2 memberaccdet trxid {trxid} "+trx.get("trxid")+" and {accdetailid} "+accdet.get("accdetailid")+ " awardmilesaccdet = "+awardmilesaccdet+" tiermilesaccdet = "+tiermilesaccdet+" frequencyaccdet = "+frequencyaccdet+
//										"\nProcess1 membertrxdet accountdetailid {accountdetailid} "+accdet.get("accdetailid")+" and {trxid} "+trxdet.get("trxid")+ " awardmilestrxdet = "+awardmilestrxdet+" tiermilestrxdet = "+tiermilestrxdet+" frequencytrxdet = "+frequencytrxdet;
//								memberaccountdetailnotupdate.add(accdet);
//							}
//						}
//					}
//				}
//			}
//		}
	}

	public void saveMemTrxMiles(Exchange exchange){
		List<Map<String, Object>> membertrxmiles = exchange.getProperty("memtrxmiles", List.class);
		if (membertrxmiles.get(0).get("awardmiles") == null)
			exchange.setProperty("awardmiles", 0);
		else
			exchange.setProperty("awardmiles", membertrxmiles.get(0).get("awardmiles"));

		if (membertrxmiles.get(0).get("tiermiles") == null)
			exchange.setProperty("tiermiles", 0);
		else
			exchange.setProperty("tiermiles", membertrxmiles.get(0).get("tiermiles"));

		if (membertrxmiles.get(0).get("frequency") == null)
			exchange.setProperty("frequency", 0);
		else
			exchange.setProperty("frequency", membertrxmiles.get(0).get("frequency"));
	}

	public void counttrxfailedcscustomtrxid(Exchange exchange){
		List<String> membertrxusedbysourcetrxid = exchange.getProperty("membertrxusedbysourcetrxid", List.class);
		membertrxusedbysourcetrxid.add(exchange.getProperty("trxid").toString());
		exchange.setProperty("membertrxusedbysourcetrxid", membertrxusedbysourcetrxid);
	}
}
