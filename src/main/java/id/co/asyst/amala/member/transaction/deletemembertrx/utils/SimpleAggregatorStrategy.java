package id.co.asyst.amala.member.transaction.deletemembertrx.utils;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * @author Ahmad Louisa Gabriella
 * @version $Revision$, Jan 22, 2021
 * @since 1.2
 */

public class SimpleAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        return newExchange;
    }
}